#[ Includes ]#
Express = require 'express'
Discord = require 'discord.js'
fs = require 'fs'
path = require 'path'
storage = require 'node-persist'
dotenv = require 'dotenv'

#[ Helper function for substring matching ]#
String::contains=(s)->~@indexOf(s)

#[ Initialize packages ]#
storage.init()
dotenv.load()
bot = new Discord.Client()
web = Express()

#[ Configuration object ]#
config = new Object
config.bennedDir = path.join(__dirname, 'benned')
config.prefix = '>'

#[ Main message handler ]#
handleMessage = (message) ->
  # break message into smaller bits
  splitContents = message.content.slice(1).split(' ')
  command = splitContents[0]
  args = splitContents.slice(1)

  # this spot intentionally left blank for non-prefixed commands

  # if our message doesn't start with the prefix then bail
  if not message.content.startsWith config.prefix then return

  switch command
    when 'echo'
      console.log "Command: #{command}, Args: #{args}"
      message.channel.sendMessage args.join(' ')
    when 'contains'
      if message.member.hasPermission("BAN_MEMBERS")
        testString = "she/her they/them he/him"
        sheTest = testString.contains("she")
        theyTest = testString.contains("they")
        heTest = testString.contains("he")
        message.channel.sendMessage("test contains she:#{sheTest}, test contains they:#{theyTest}, test contains he:#{heTest}")
    when 'groups'
      if message.member.hasPermission("BAN_MEMBERS")
        for role in message.member.roles.array()
          message.channel.sendMessage "#{role.name}: #{role.id}"
    when 'benned'
      if message.member.hasPermission("BAN_MEMBERS")
        files = fs.readdirSync config.bennedDir
        choice = files[Math.floor(Math.random() * files.length)]
        message.channel.sendFile path.join(config.bennedDir, choice)
    when 'getout'
      message.channel.sendFile path.join(__dirname, 'getout.gif')
    when 'pronoun', 'pronouns'
      # check if args are a user ID or not
      if args.join(' ').match(/<@!?\d+>/) or args.length is 0
        if args.length is 0
          id = message.author.id
        else
          id = args.join(' ').match(/<@!?(\d+)>/)[1]
        # we are checking the pronouns of a user
        pronoun = storage.getItemSync "pronoun_#{id}"
        if pronoun
          message.channel.sendMessage "<@!#{id}>'s pronoun(s): #{pronoun}"
        else
          message.channel.sendMessage "<@!#{id}> hasn't specified pronouns."
      else
        # we are setting the pronouns of the caller
        pronoun = args.join(' ')
        # set up an array to gather roles that need to be removed
        rolesToRemove = []
        # check each role for a pronoun
        for role in message.member.roles.array()
          for id in [process.env.ROLE_SHE, process.env.ROLE_THEY, process.env.ROLE_HE]
            # if the role is a pronoun, prepare to remove it
            if role.id is id
              rolesToRemove.push(role)
              break
        if pronoun is "none"
          # remove all pronoun roles
          message.member.removeRoles(rolesToRemove).then ->
            storage.removeItem("pronoun_#{message.author.id}").then ->
              message.channel.sendMessage "Ok, I've removed your pronouns."
        else
          storage.setItem("pronoun_#{message.author.id}", pronoun).then ->
            # remove all pronoun roles
            message.member.removeRoles(rolesToRemove).then ->
              # add pronoun roles based on some matching
              rolesToAdd = []
              if pronoun.contains("she") or pronoun.contains("her")
                rolesToAdd.push process.env.ROLE_SHE
              if pronoun.contains("they") or pronoun.contains("them")
                rolesToAdd.push process.env.ROLE_THEY
              if pronoun.match(/\bhe\b/g) or pronoun.match(/\bhis\b/g)
                rolesToAdd.push process.env.ROLE_HE
              message.member.addRoles(rolesToAdd).then ->
                # then let the user know
                message.channel.sendMessage "Ok, I've set your pronouns as #{pronoun}"

#[ Set up the discord bot ]#
bot.on 'ready', ->
  console.log 'Discord ready!'
bot.on 'message', handleMessage
bot.login process.env.DISCORD_TOKEN

#[ Set up the web server ]#
web.get '/', (req, res) ->
  res.send 'Nothing to see.'
web.get '/benned', (req, res) ->
  files = fs.readdirSync config.bennedDir
  res.render 'benned', {
    title: 'BrdBot | Benned'
    files: files
  }
web.set 'view engine', 'pug'
web.use '/benned/img', Express.static(config.bennedDir)
web.listen 3000, ->
  console.log 'Webserver ready!'
